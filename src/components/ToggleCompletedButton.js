import { useState } from 'react';
import ToggleButton from 'react-bootstrap/ToggleButton';
import Badge from 'react-bootstrap/Badge';

function ToggleCompletedButton(props) {
  const [checked, setChecked] = useState(false);

  // TODO bring toggling functionality to parent component
  return (
    <>
      <ToggleButton
        className='m-3'
        id={props.id}
        variant={checked ? 'secondary' : 'primary'}
        checked={checked}
        value='1'
        type='checkbox'
        onChange={() => setChecked(!checked)}
      >
        {checked ? 'Set to-do unfinished' : 'Set to-do completed'}
      </ToggleButton>
      <Badge
        className='p-2'
        text={checked ? '' : 'dark'}
        bg={checked ? 'success' : 'warning'}
      >
        {checked ? 'To-do Completed!' : 'To-do unfinished'}
      </Badge>
    </>
  );
}

export default ToggleCompletedButton;
