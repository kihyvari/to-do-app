import React from 'react';
import Accordion from 'react-bootstrap/Accordion';
import { MdDelete } from 'react-icons/md';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import TodoButton from './TodoButton';
import ToggleCompletedButton from './ToggleCompletedButton'

function TodoContainer({ todos, handleDelete }) {

  // TODO bring to-do completed state handling here 
  // for adding 'task done' icon etc. to Accordion.Header

  return todos.map((todo, i) => {
    return (
      <Accordion key={todo.id}>
        <Accordion.Item eventKey={todo.id}>
          <Accordion.Header>{todo.title}</Accordion.Header>
          <Accordion.Body>
            <Row>{todo.body}</Row>
            <Row>
              <Col>
                <TodoButton
                  handleClick={() => handleDelete(todo.id)}
                  variant='danger'
                  content='Delete To-do'
                  classNameProp='m-3'
                  type='button'
                  icon={<MdDelete/>}
                />
              </Col>
              <Col>
                <ToggleCompletedButton id={todo.id} />
              </Col>
            </Row>
          </Accordion.Body>
        </Accordion.Item>
      </Accordion>
    );
  });
}

export default TodoContainer;
