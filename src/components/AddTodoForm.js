import React from 'react';
import Form from 'react-bootstrap/Form';
import TodoButton from './TodoButton';

function AddTodoForm({ handleAdd, todos }) {

  // TODO clear fields after submit
  return (
    <Form onSubmit={handleAdd}>
      <Form.Group className='mb-3' controlId='title'>
        <Form.Label>Add To-do</Form.Label>
        <Form.Control
          name='todo-title'
          placeholder='Enter title'
          type='text'
          value={todos.title}
        />
        <Form.Text className='text-muted'></Form.Text>
      </Form.Group>
      <Form.Group className='mb-3' controlId='body'>
        <Form.Label>To-do description</Form.Label>
        <Form.Control
          as='textarea'
          name='todo-body'
          placeholder='Enter description'
          style={{ height: '80px' }}
          type='text'
          value={todos.body}
        />
      </Form.Group>
      <TodoButton
        // This doesn't seem legit
        handleClick={undefined}
        variant='primary'
        content='Add to-do'
        classNameProp=''
        type='submit'
        icon=''
      />
    </Form>
  );
}

export default AddTodoForm;
