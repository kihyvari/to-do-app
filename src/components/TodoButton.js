import React from 'react';
import Button from 'react-bootstrap/Button';

// somewhat reusable component
function TodoButton({handleClick, content, variant, icon, classNameProp, type }) {
  return (
    <Button
      onClick={handleClick}
      className={classNameProp}
      variant={variant}
      type={type}
    >
     {content ? content : ''}
     {icon ? icon : ''}
    </Button>
  );
}
export default TodoButton;
