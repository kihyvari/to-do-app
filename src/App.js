import './App.css';
import { useState, useEffect } from 'react';
import axios from 'axios';
import TodoContainer from './components/TodoContainer';
import AddTodoForm from './components/AddTodoForm';
import 'bootstrap/dist/css/bootstrap.min.css';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

function App() {
  const [todos, setTodos] = useState(null);
  // TODO error handling
  // eslint-disable-next-line no-unused-vars
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  // Gets mock data
  useEffect(() => {
    const getTodos = async () => {
      try {
        const response = await axios.get(
          'https://jsonplaceholder.typicode.com/posts/?_limit=8'
        );
        setTodos(response.data);
        setError(null);
      } catch (error) {
        setError(error.message);
        setTodos(null);
      } finally {
        setLoading(false);
      }
    };
    getTodos();
  }, []);

  // Adds a new to-do
  const handleAdd = (e) => {
    e.preventDefault();

    // Checks if there's any to-dos
    const lastTodo = todos.length > 0 ? todos.slice(-1) : null;
    // TODO actually unique ids
    const newId = lastTodo !== null ? lastTodo[0].id + 1 : 1;
    const newTodo = {
      userId: 1,
      id: newId,
      title: e.target.title.value,
      body: e.target.body.value,
    };
    setTodos((prevTodos) => [...prevTodos, newTodo]);
  };

  // Removes a to-do
  const handleDelete = (id) => {
    const remainingTodos = todos.filter((todo) => todo.id !== id);
    setTodos(remainingTodos);
  };

  // TODO insert loading spinners
  return (
    <div className='App'>
      <h1>To-do App</h1>
      <Container>
        <Row className='justify-content-md-center'>
          <Col xs lg='8'>
            {!loading && (
              <TodoContainer
                handleDelete={handleDelete}
                todos={todos}
              />
            )}
            {!loading && <AddTodoForm handleAdd={handleAdd} todos={todos} />}
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default App;
